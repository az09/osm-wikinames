import argparse
import logging.handlers
import time
import traceback
from concurrent.futures import ThreadPoolExecutor, as_completed

import psycopg2
import sqlite3
import psycopg2.extras
import pywikibot
from filelock import FileLock, Timeout
from pywikibot.exceptions import InvalidTitle, MaxlagTimeoutError

DEFAULT_CACHE_FILE = 'wiki-cache.db'
DEFAULT_LOCK_FILE = '/tmp/osm-wikinames.lock'

wiki_repo = pywikibot.Site("wikidata", "wikidata").data_repository()

query_count_missing = """
select count(*)
from {0}
where tags->'name:{1}' is null and {1}wikinamecheck is null and (tags->'wikidata' is not null or tags->'wikipedia' is not null)
{2}
"""

query_missing = """
select osm_id, {0}wikiname, "name", tags->'wikidata' as wikidata, tags->'wikipedia' as wikipedia
from {1}
where tags->'name:{0}' is null and {0}wikinamecheck is null and (tags->'wikidata' is not null or tags->'wikipedia' is not null)
{2}
limit {3}
"""

update_query_fill_template = """
update {0} set {1}wikinamecheck=1, {1}wikiname=%(wikiname)s where osm_id={2}
"""

update_query_empty_template = """
update {0} set {1}wikinamecheck=1 where osm_id={2}
"""

logger = None


def retry_on_error(timeout_in_seconds=3*60):
    def decorate(func):
        def call(*args, **kwargs):
            retries = 5
            while retries > 0:
                try:
                    result = func(*args, **kwargs)
                except MaxlagTimeoutError:
                    retries = retries - 1
                    logger.warning('Timeout error, retrying')
                    time.sleep(timeout_in_seconds)
                    continue
                return result
            raise Exception('Exhausted retries for timeout, quitting')
        return call
    return decorate


def setup_logger(logging_level=logging.INFO):
    """
    Simple logger used throughout whole code - logs both to file and console
    """
    logger = logging.getLogger('osm-wikinames')
    logger.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.handlers.TimedRotatingFileHandler(filename='osm-wikinames.log', when='midnight', interval=1,
                                                   encoding='utf-8')
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    ch = logging.StreamHandler()
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


@retry_on_error()
def get_wikidataname(language, wikidata):
    wikidataname = None
    try:
        wikidata_entry = pywikibot.ItemPage(wiki_repo, wikidata)
    except InvalidTitle:
        logger.warning('Invalid title: {0}'.format(wikidata))
        return None

    if wikidata_entry.pageid != 1 and wikidata_entry.exists():
        if language in wikidata_entry.text['labels']:
            wikidataname = wikidata_entry.text['labels'][language]
        else:
            try:
                wikidataname = wikidata_entry.getSitelink('{0}wiki'.format(language))
            except pywikibot.exceptions.NoPage:
                pass
    return wikidataname


def get_wikipedianame(language, wikipedia):
    if wikipedia.startswith('{0}:'.format(language)):
        link = wikipedia[3:]
        has_paren = link.find(' (')
        if has_paren > -1:
            link = link[:link.index(' (')]
        return link
    return None


def process_table_chunk(language, table_name, cursor, cache_cursor, chunk_size, total_modulo, current_modulo):
    modulo_info = ''
    modulo_query_filter = ''
    if total_modulo > 1:
        modulo_info = '[thread {0}/{1}] '.format(current_modulo+1, total_modulo)
        modulo_query_filter = 'AND MOD(ABS(osm_id), {0}) = {1}'.format(total_modulo, current_modulo)

    cursor.execute(query_missing.format(language, table_name, modulo_query_filter, chunk_size))
    missing_names = cursor.fetchall()
    if len(missing_names) == 0:
        return 0
    for missing_name in missing_names:
        wikiname = None
        found_in_cache = False
        if missing_name['wikidata']:
            cache_cursor.execute('SELECT name FROM wikidata WHERE wikidata = ?', (missing_name['wikidata'],))
            cache_row = cache_cursor.fetchone()
            if cache_row is not None:
                wikiname = cache_row[0]
                found_in_cache = True
            else:
                wikiname = get_wikidataname(language, missing_name['wikidata'])
        if wikiname is None and missing_name['wikipedia']:
            wikiname = get_wikipedianame(language, missing_name['wikipedia'])
        logger.debug('{0}Processing {1} (osm: {2}, wikidata: {3}, wikipedia: {4})...found {5}{6}'.format(
            modulo_info, missing_name["name"], missing_name['osm_id'], missing_name['wikidata'],
            missing_name['wikipedia'], wikiname if wikiname else 'n/a',
            ' (from cache)' if found_in_cache else ''
        ))
        params = {}
        if wikiname:
            update_query = update_query_fill_template.format(table_name, language, missing_name['osm_id'])
            params = {'wikiname': wikiname}
            if not found_in_cache and missing_name['wikidata'] is not None:
                cache_cursor.execute('INSERT INTO wikidata VALUES(?, ?)', (missing_name['wikidata'], wikiname))
        else:
            update_query = update_query_empty_template.format(table_name, language, missing_name['osm_id'])
        cursor.execute(update_query, params)
    return len(missing_names)


def process_table_thread(conn, cache_file, language, table_name, chunk_size, total_modulo, current_modulo):
    modulo_info = ''
    modulo_query_filter = ''
    if total_modulo > 1:
        modulo_info = '[thread {0}/{1}] '.format(current_modulo+1, total_modulo)
        modulo_query_filter = 'AND MOD(ABS(osm_id), {0}) = {1}'.format(total_modulo, current_modulo)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(query_count_missing.format(table_name, language, modulo_query_filter))
    count_total = cursor.fetchone()[0]
    processed = 0

    cache = sqlite3.connect(cache_file)
    cache.isolation_level = None
    cache_cursor = cache.cursor()

    logger.info('{0}Found {1} entries from table {2} to process'.format(modulo_info, count_total, table_name))
    processed += process_table_chunk(language, table_name, cursor, cache_cursor, chunk_size, total_modulo, current_modulo)
    conn.commit()
    while processed < count_total:
        logger.info('{0}{1}/{2} processed in table {3}'.format(modulo_info, processed, count_total, table_name))
        chunks_processed = process_table_chunk(language, table_name, cursor, cache_cursor, chunk_size, total_modulo, current_modulo)
        processed += chunks_processed
        conn.commit()
        cache.commit()
        if chunks_processed == 0:
            break
    cursor.close()
    conn.commit()
    cache.commit()


def process_table(conn, cache_file, language, table_name, chunk_size, threads):
    all_futures = []
    with ThreadPoolExecutor(max_workers=threads) as executor:
        for i in range(threads):
            future = executor.submit(process_table_thread, conn, cache_file, language, table_name, chunk_size, threads, i)
            all_futures.append(future)
        # Wait for all
        for i, future in enumerate(as_completed(all_futures)):
            ex = future.exception()
            if ex is not None:
                str = traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)
                logger.exception('Exception in thread {0}/{1}'.format(i+1, threads))
                logger.exception("".join(str))


def sync_postgis_table_to_cache(conn, cache_file, language, table_name):
    cache = sqlite3.connect(cache_file)
    cache_cursor = cache.cursor()
    cache_cursor.execute('DELETE FROM wikidata')
    logger.info('Syncing table {0} to cache. This can take lot of time, depending on size of your database. Be patient'.format(table_name))
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("SELECT tags->'wikidata' AS wikidata, {0}wikiname AS wikiname FROM {1} WHERE tags->'wikidata' IS NOT NULL AND {0}wikinamecheck='1'".format(language, table_name))
    processed = 0
    for row in cursor:
        wikidata = row['wikidata']
        name = row['wikiname']
        cache_cursor.execute('SELECT * FROM wikidata WHERE wikidata = ?', (wikidata,))
        if cache_cursor.fetchone() is None:
            cache_cursor.execute('INSERT INTO wikidata VALUES(?,?)', (wikidata, name))
        processed += 1
        if processed % 1000 == 0:
            logger.info('Synced {0} entries'.format(processed))
            cache.commit()
    cache.commit()
    logger.info('Synced {0} entries'.format(processed))


def sync_postgis_to_cache(conn, cache_file, language):
    sync_postgis_table_to_cache(conn, cache_file, language, 'planet_osm_point')
    sync_postgis_table_to_cache(conn, cache_file, language, 'planet_osm_line')
    sync_postgis_table_to_cache(conn, cache_file, language, 'planet_osm_roads')
    sync_postgis_table_to_cache(conn, cache_file, language, 'planet_osm_polygon')


def main():
    global logger

    parser = argparse.ArgumentParser(
        description='OSM-Wikinames - helper tool to make your Mapnik server internationalized')
    parser.add_argument('-l', '--language', required=True,
                        help='2 or 3-letter language code. Read a documentation how to set it up.')
    parser.add_argument('-d', '--database', default='gis', help='Database name to connect to')
    parser.add_argument('-t', '--threads', default=1, type=int, help='Number of parallel threads to fetch items from wikidata. Default: 1')
    parser.add_argument('--chunk-size', default=100, type=int, help='Number of entries to process before commiting them. Default: 100')
    parser.add_argument('--cache-file', default=DEFAULT_CACHE_FILE,
                        help='Name of file that will keep cache of all previously cached entries')
    parser.add_argument('--lock-file', default=DEFAULT_LOCK_FILE,
                        help='Name of file that will keep lock of running process, so we don\'t start multiple processes')
    parser.add_argument('--sync-postgis-to-cache', action='store_true', help='This option will clear cache and it will sync whatever is currently in PostGIS database to cache')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
    parser.add_argument('--version', action='version', version='OSM-Wikinames 0.1')

    args = parser.parse_args()

    logger = setup_logger(logging_level=logging.DEBUG if args.verbose else logging.INFO)
    lock = FileLock(args.lock_file, timeout=0)
    try:
        with lock:
            conn = psycopg2.connect(database=args.database)

            # Prepare cache
            cache = sqlite3.connect(args.cache_file)
            cache_cursor = cache.cursor()
            cache_cursor.execute('CREATE TABLE IF NOT EXISTS wikidata(wikidata TEXT NOT NULL, name TEXT, PRIMARY KEY("wikidata"))')
            cache.commit()

            logger.info('Connected to database')
            if args.sync_postgis_to_cache:
                sync_postgis_to_cache(conn, args.cache_file, args.language)
            else:
                process_table(conn, args.cache_file, args.language, 'planet_osm_point', chunk_size=args.chunk_size, threads=args.threads)
                process_table(conn, args.cache_file, args.language, 'planet_osm_polygon', chunk_size=args.chunk_size, threads=args.threads)
                process_table(conn, args.cache_file, args.language, 'planet_osm_line', chunk_size=args.chunk_size, threads=args.threads)
                process_table(conn, args.cache_file, args.language, 'planet_osm_roads', chunk_size=args.chunk_size, threads=args.threads)
    except Timeout:
        logger.error('Unable to obtain lock, seems that process is already running')


if __name__ == '__main__':
    main()
