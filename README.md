# OSM Wikinames

Project for internalization of OpenStreetMap mapnik-based maps using Wikidata and Wikipedia articles data.

If you just want to see results as fancy GIFs, go to [showcase page](showcase.md).

## How it works

If you have carto-based map, this project can help you translate map to languages you want.

It does so by:
1. Making additional columns in your PostGIS database (explained in [installation](#installation) section)
2. Leveraging `wikidata` and `wikipedia` tags of OSM entities to get their translation into those columns (this script)
3. Adjusting mapnik renderer to include new columns (also explained in [installation](#installation) section)

## Installation

From now on, replace all `<XX>` with language of your choice (`de` for German etc.). I guess any 2 or 3-letter combination will do, but maybe try to align with [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).

### Before import

#### Add columns to PostGIS
First, you need to have OSM Mapnik based map installation. Start with [swtich2osm](https://switch2osm.org/serving-tiles/)
website if you need help starting.

However, you need to patch openstreetmap-carto style a bit. Before starting,
add following line to your `openstreetmap-carto.style` file:
```
node,way   <XX>wikiname          text  linear
node,way   <XX>wikinamecheck     text  linear
```

You can add it at the end of this file or somewhere in the middle. Those two command will tell `osm2pgsql`
to add two additional columns to your PostGIS database.
You need to replace all `<XX>` in above definition to your language of choice.

### Adjust style to read new columns

Mapnik style is changed to fetch data from wiki name if it exists, then from `name:<XX>` tag if that one exists,
and finally from `name` tag.

Open `project.mml` and replace every instance of `name` with this:
```sql
COALESCE("name:<XX>", "<XX>wikiname", name, '') AS name
```

In current version of `openstreetmap-carto` project (5.2.0), I found 32 changes that needs to be made.
Once you are done, generate mapnik XML with something like:
```shell script
carto project.mml > mapnik.xml
```

### After import

#### Add indexes

It is not needed, but this script will run much faster if you add indexes to your DB. To add them,
once you finish import, add them with this SQL snippet:
```sql
CREATE INDEX IF NOT EXISTS planet_osm_point_<XX>wikinamecheck ON planet_osm_point(osm_id) wHERE tags->'name:<XX>' IS NULL AND <XX>wikinamecheck IS NULL AND (tags->'wikidata' IS NOT NULL OR tags->'wikipedia' IS NOT NULL);
CREATE INDEX IF NOT EXISTS planet_osm_line_<XX>wikinamecheck ON planet_osm_line(osm_id) wHERE tags->'name:<X>>' IS NULL AND <XX>wikinamecheck IS NULL AND (tags->'wikidata' IS NOT NULL OR tags->'wikipedia' IS NOT NULL);
CREATE INDEX IF NOT EXISTS planet_osm_roads_<XX>wikinamecheck ON planet_osm_roads(osm_id) wHERE tags->'name:<XX>' IS NULL AND <XX>wikinamecheck IS NULL AND (tags->'wikidata' IS NOT NULL OR tags->'wikipedia' IS NOT NULL);
CREATE INDEX IF NOT EXISTS planet_osm_polygon_<XX>wikinamecheck ON planet_osm_polygon(osm_id) wHERE tags->'name:<X>>' IS NULL AND <XX>wikinamecheck IS NULL AND (tags->'wikidata' IS NOT NULL OR tags->'wikipedia' IS NOT NULL);
```

Again, change all `<XX>` references to language of your choice!

#### Run script in cron

Once you finish process of importing data, you can start this script.

Try it out first, depending on size of your import, it can take a looong time
(Europe is around 400k objects per table)! If you have huge amount of objects,
you can speed it up a bit (look at FAQ for details).

You can try it with:
```shell script
cd src
# Change language in main function to language of your choice!
python3 main.py --language=<XX>
```

It will tell you how many entries it needs to process and it will start doing them all.
While fetching data, it will store them locally in cache, so if you ever recreate postGIS database,
it will just get data from cache.

Script also knows how to pick up where it left, so don't worry if it crashes, just run it again.
However, if you *do* see it crashes, please file a bug report!

#### Continuous update

Once you fill your DB for the first time, and if you plan to
[refresh your PostGIS continuously](https://wiki.openstreetmap.org/wiki/HowTo_minutely_hstore),
you can set up a cron task to refresh DB from time to time. Just run `python3 main.py --language=<XX>` again.
Script is smart enough to know if another instance is running, so you can put any interval you like in your cron.

Since there is caching, lot of existing entries will already exist in cache and it will not use network at all.

If you also want to re-render/expire your tiles, run this script immediatelly after you run `osm2pgsql`, but before you call `render_expired`.

## Known issues and limitations

* Sometimes wikidata and/or wikipedia have some subject in parenthesis
(e.g. occitan wikipedia defines Paris as "[París (França)](https://oc.wikipedia.org/wiki/Par%C3%ADs_(Fran%C3%A7a))".
This scripts will remove everything in parenthesis including those too (e.g. it will leave "Paris" only). This is default behaviour, but it could be wrong, depending on the language.
* Sometimes, roads have short names (e.g. "26"), but this script will give them much longer names as Wikipedia usually have longer names for them (e.g. "[Route magistrale 26](https://fr.wikipedia.org/wiki/Route_magistrale_26_(Serbie))"). You can remove `roads` table from internalization, or you can provide proper `name:<XX>` value instead.

## Showcase

On [showcase page](showcase.md), you can see various examples of this script in action.
Beware that those GIFs are huge (>20MB per GIF).

## FAQ

* This script is slow, can it go faster?

  Yes. Use `--threads=N` to use more than one thread to run wikidata fetching.
  Note that you are still network bound, so don't go wild with this. 4 or 8 should be OK.

* My database is not named `gis`. Can I chage that?

  Yes. Use argument `--database=foo` to change name of database you are connecting to.

* I have data in PostGIS, but I lost my cache file. Is there any way to recreate my cache?

  Yes. Use `--sync-postgis-to-cache` command line argument to go in reverse and recreate your cache.
  Note that this will *delete* all existing data in cache!

## Alternatives

For general map internalization, check out [Map internalization wiki](https://wiki.openstreetmap.org/wiki/Map_internationalization).

I am not aware of any more specific examples of internalization using Mapnik raster tiles.
If you do know of anything, please let me know to add it here.