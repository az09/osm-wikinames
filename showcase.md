## Showcase

Here are some example that tries to convey what and how much is gained with this approach

This is example with `name:sr` and Serbian wikidata and wikipedia.
Even if you are not native speaker, you can watch changes in GIF.
Rule of the thumb is - if you see cyrillic script, it is Serbian (so therefore - translated).

All GIFs can be clicked for better view.

## Zoom 5

Not much can be seen on zoom 5, as `name:sr` is already covered pretty much. If you squint a bit, you will see
some differences though:

![Zoom level 5](showcase/z5.gif)

## Zoom 6

Even in southeast Europe, `name:sr` has pretty good coverage. Don't look at Bulgaria, as it is cyrillic
already, so it is hard to see differences. Romania, Italy and Turkey do have some notable changes.

![z6-1](showcase/z6-1.gif)

For some reason, `name:sr` is more prominent in Spain than in Italy. Probably some enthusiast that covered Spain
and didn't cover Italy:)

![z6-2](showcase/z6-2.gif)

It is interesting that `England` do not have Serbian version in OSM.
Other than that, central Europe is OK on this zoom level, even without this script.

![z6-3](showcase/z6-3.gif)

## Zoom 7

First let's zoom to ex-Yugoslavia. Note that it is almost the same, mostly because `name:sr` is already added all around.

![z7-exy](showcase/z7-exyu.gif)

But look at this. Turkey has really low `name:sr` coverage and difference is striking!
![z7-turkey](showcase/z7-turkey.gif)

Same with Northern Italy:
![z7-northitaly](showcase/z7-northitaly.gif)

Let's move quickly over couple of others on zoom level 7:
![z7-england](showcase/z7-england.gif)
![z7-benelux](showcase/z7-benelux.gif)
![z7-iberian](showcase/z7-iberian.gif)

## Zoom 8

I will showcase level 8 with same ex-yu version that shows there are not lot of changes locally:

![z8-bosnia-montenegro-kosovo.gif](showcase/z8-bosnia-montenegro-kosovo.gif)

However, just look at Poland and Lombardy, even smaller places are translated now:
![z8-poland](showcase/z8-poland.gif)
![z7-lombardy](showcase/z8-lombardy.gif)

## Zoom 9

This is highest zoom that benefits from data in wikidata and wikipedia. After this point, we get into diminishing returns territory.
Here is ex-yu republic of Slovenia, that benefited a lot from wikidata:
![z9-slovenia](showcase/z9-slovenia.gif)

For some reason, Czech republic has also really good coverage on Wiki project, seems we have lot of articles in Serbian wiki for Czech.
Not all countries are this good, and Czech is among better ones.
![z9-czech](showcase/z9-czech.gif)

Another area with good coverage is around Berlin:
![z9-berlin](showcase/z9-berlin.gif)

